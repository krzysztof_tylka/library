package pl.com.tt.operations;

import pl.com.tt.entity.EntityManager;

public class OperationManager extends EntityManager {

    private int operationsCount = 0;
    private Operation operations[] = new Operation[500];

    public void add(Operation operation) {

        add(operation, operations, findFirstEmptyPlaceIdx(operations));
        operationsCount++;
    }

    public void remove(int... operationsIds) {

        int removeCount = remove(operations, operationsIds);
        this.operationsCount -= removeCount;
    }

    public Operation[] findByTypeOfOperation(Operation[] operations, String text) {

        int count = 0;
        Operation[] findedOperations = new Operation[operations.length];
        for (Operation operation : operations) {
            if (operation != null) {
                if (operation.getTypeOfOperation() == (text)) {
                    findedOperations[count] = operation;
                    count++;
                }
            }
        }
        return findedOperations;
    }

    public Operation[] getOperations() {
        return operations;
    }

    public int getOperationsCount() {
        return operationsCount;
    }
}
