package pl.com.tt.operations;

import pl.com.tt.entity.Entity;
import pl.com.tt.items.Book;
import pl.com.tt.items.Newspaper;
import pl.com.tt.persons.Client;
import pl.com.tt.persons.Employee;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class Operation extends Entity {

    public static final String LENDING = "lending";
    public static final String RETURNING = "returning";

    private Date dateOfLending;
    private Date dateOfReturning;
    private Book[] books;
    //    private Newspaper[] newspapers;
    private Client client;
    //    private Employee employee;
    private String notes;
    private String typeOfOperation;

    public Operation(Book[] books, Client client, String notes, String typeOfOperation) {

        this.dateOfLending = Calendar.getInstance().getTime();
        this.books = books;
//        this.newspapers = newspapers;
        this.client = client;
//        this.employee = employee;
        this.notes = notes;
        this.typeOfOperation = LENDING;
    }

    public Date getDateOfLending() {
        return dateOfLending;
    }

    public Date getDateOfReturning() {
        return dateOfReturning;
    }

    public Book[] getBooks() {
        return books;
    }

//    public Newspaper[] getNewspapers() {
//        return newspapers;
//    }

    public Client getClient() {
        return client;
    }

//    public Employee getEmployee() {
//        return employee;
//    }

    public String getNotes() {
        return notes;
    }

    public String getTypeOfOperation() {
        return typeOfOperation;
    }

}


