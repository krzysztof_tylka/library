package pl.com.tt.utils;

import pl.com.tt.enums.ANSIColors;

import java.nio.charset.Charset;

public class TextsPL {

    public static final String AT_LEAST_ONE_CHAR = " musi zawierać przynajmniej jeden znak. ";
    public static final String AUTHOR = "autora";
    public static final String AUTHOR1 = "Autor";
    public static final String CITY = "miasto";
    public static final String CITY1 = "Miasto";
    public static final String DATE_OF_REGISTRATION = "data rejestracji";
    public static final String EMAIL = "adres email";
    public static final String EMAIL1 = "Adres email";
    public static final String ISBN = "ISBN";
    public static final String MONTH = "miesiąc";
    public static final String MONTH1 = "Miesiąc";
    public static final String NAME = "imię";
    public static final String NAME1 = "Imię";
    public static final String NO_RESULTS = "Brak wyników wyszukiwania.";
    public static final String POSITION = "pozycja";
    public static final String PUBLISHER = "wydawca";
    public static final String PUBLISHER1 = "Wydawca";
    public static final String RELEASE_NUMBER = "numer wydania";
    public static final String RELEASE_NUMBER1 = "Numer wydania";
    public static final String SEARCHING_TEXT = "frazę wyszukiwania";
    public static final String SEARCHING_TEXT1 = "fraza wyszukiwania";
    public static final String SURNAME = "nazwisko";
    public static final String SURNAME1 = "Nazwisko";
    public static final String STREET = "ulica";
    public static final String STREET1 = "Ulica";
    public static final String STREET_NUMBER = "numer domu";
    public static final String STREET_NUMBER1 = "Numer domu";
    public static final String TITLE = "tytuł";
    public static final String TITLE1 = "Tytuł";
    public static final String TRY_AGAIN = "Spróbuj ponownie.";
    public static final String TYPE_OF_ERROR = "Rodzaj błędu: ";
    public static final String TYPE_OF_NEWSPAPER = "rodzaj czasopisma";
    public static final String YEAR = "rok";
    public static final String YEAR1 = "Rok";
    public static final String YEAR_OF_PUBLISH = "rok wydania";
    public static final String YEAR_OF_PUBLISH1 = "Rok wydania";
    public static final String UTF = "UTF-8";
    public static final String UNDER_CONSTRUCTION = ANSIColors.ANSI_BLUE + "--- under construction ---" + ANSIColors.ANSI_RESET;

}
