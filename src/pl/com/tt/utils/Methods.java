package pl.com.tt.utils;

import static pl.com.tt.enums.ANSIColors.*;

public class Methods {

    public static final String lineSeparator = System.lineSeparator();

    public static void printNextLine() {
        System.out.print(System.lineSeparator());
    }

    public static void printNoSearchingResultsText() {
        System.out.println("Brak wyników wyszukiwania.");
    }

    public static void printRequestForMakingChoice() {
        printNextLine();
        System.out.println(ANSI_YELLOW + "Wybierz jedną z opcji:" + ANSI_RESET);
    }
}

