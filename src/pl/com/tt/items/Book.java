package pl.com.tt.items;

import java.io.Serializable;

public class Book extends Item implements Comparable<Book>, Serializable {

    private int releaseNumber;
    private String author;

    public Book(String title, String author, int releaseNumber, int yearOfPublish, int isbn) {

        super(title, yearOfPublish, isbn);
        this.releaseNumber = releaseNumber;
        this.author = author;
    }

    public int getReleaseNumber() {
        return releaseNumber;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Book id=" + id +
                " {title=" + title +
                ", author=" + author +
                ", releaseNumber=" + releaseNumber +
                ", yearOfPublish=" + yearOfPublish +
                ", isbn=" + isbn +
                ", status=" + status +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        int titleComparation = (this.title).compareTo(o.title);
        if (titleComparation > 0) {
            return 1;
        } else if (titleComparation < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}

