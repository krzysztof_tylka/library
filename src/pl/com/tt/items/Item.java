package pl.com.tt.items;

import pl.com.tt.entity.Entity;

public abstract class Item extends Entity {

    public static final String AVAILABLE = "available";
    public static final String LENDED = "lended";
    public static final String BLOCKED = "blocked";

    protected String title;
    protected int yearOfPublish;
    protected int isbn;
    protected String status;

    public Item(String title, int yearOfPublish, int isbn) {

        this.title = title;
        this.yearOfPublish = yearOfPublish;
        this.isbn = isbn;
        this.status = AVAILABLE;
    }

    public Item() {

    }

    public String getTitle() {
        return title;
    }

    public int getYearOfPublish() {
        return yearOfPublish;
    }

    public int getIsbn() {
        return isbn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }






}
