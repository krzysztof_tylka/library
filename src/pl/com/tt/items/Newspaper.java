package pl.com.tt.items;

import pl.com.tt.enums.TypeOfNewspaper;

public class Newspaper extends Item {

    private String publisher;
    private int month;
    private String typeOfNewspaper;

    public Newspaper(String title, String publisher, int yearOfPublish, int month, int isbn) {
        super(title, yearOfPublish, isbn);
        this.publisher = publisher;
        this.month = month;
        this.typeOfNewspaper = TypeOfNewspaper.WEEKLY_NEWSPAPER.getTypeOfNewspaper();
    }

    public String getPublisher() {
        return publisher;
    }

    public int getMonth() {
        return month;
    }

    public String getTypeOfNewspaper() {
        return typeOfNewspaper;
    }

    @Override
    public String toString() {
        return "Newspaper id=" + id + " {" +
                "publisher='" + publisher +
                ", month='" + month +
                ", typeOfNewspaper='" + typeOfNewspaper +
                ", title='" + title +
                ", yearOfPublish=" + yearOfPublish +
                ", isbn=" + isbn +
                ", status='" + status +
                '}';
    }

}
