package pl.com.tt.items;

import pl.com.tt.entity.EntityManager;

public class ItemManager extends EntityManager {

    public static Item[] findByStatusAndTitle(Item[] items, String status, String title) {
        Item[] findingitems = new Item[items.length];
        int count = 0;
        for (Item item : items) {
            if (item != null) {
                if (item.getStatus().equals(status) && item.getTitle().equals(title)) {
                    findingitems[count] = item;
                    count++;
                }
            }
        }
        return findingitems;
    }

    public static Item[] findByTitle(Item[] items, String title) {
        Item[] findingitems = new Item[items.length];
        int count = 0;
        for (Item item : items) {
            if (item != null) {
                if (checkTitle(title, item)) {
                    findingitems[count] = item;
                    count++;
                }
            }
        }
        return findingitems;
    }

    //    TODO use list
    public static Item[] findByStatus(Item[] items, String status) {
        Item[] findingItems = new Item[items.length];
        int count = 0;
        for (Item Item : items) {
            if (Item != null && Item.getStatus().equals(status)) {
                findingItems[count] = Item;
                count++;
            }
        }
        return findingItems;
    }

    protected static boolean checkTitle(String text, Item item) {
        if (item.getTitle() == null) { return false;}
        return item.getTitle().toLowerCase().contains(text.toLowerCase());
    }

    public static void lendItems(Item[] items) {
        for (Item item : items) {
            item.setStatus(Item.LENDED);
        }
    }

    public static void returnItems(Item[] items) {
        for (Item item : items) {
            item.setStatus(Item.AVAILABLE);
        }
    }

}
