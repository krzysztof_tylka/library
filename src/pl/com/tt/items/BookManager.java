package pl.com.tt.items;

import pl.com.tt.entity.Entity;
import pl.com.tt.exceptions.UniquenessException;
import pl.com.tt.persons.Client;
import pl.com.tt.raports.RaportDataSourceInterface;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BookManager extends ItemManager implements RaportDataSourceInterface {

    private int booksCount = 0;
    private Collection<Book> books = new ArrayList<Book>();

    public BookManager() throws IOException {

        /** wczytywanie książzek z pliku tekstowego**/
//        try {
//
//            File file = new File("books.txt");
//
//            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
//                String textLine;
//                while ((textLine = br.readLine()) != null) {
//                    String[] bookData = textLine.split(",");
//                    if (bookData.length == 5) {
//                        Book book = new Book(bookData[0], bookData[1], Integer.parseInt(bookData[2]), Integer.parseInt(bookData[3]), Integer.parseInt(bookData[4]));
//                        book.setId(booksCount);
//                        books.add(book);
//                        booksCount++;
//                    }
//
//                }
//            }
//        } catch (FileNotFoundException e) {
//            System.out.println("brak pliku bazowego books.txt");
//        }

        /** deserializacja danych**/
        try {
            booksDeserialization();
        } catch (ClassNotFoundException e) {
            System.out.println("obiekt nie może być zaimportowany - błędna klasa");
        }
    }

    public void add(Book book) throws UniquenessException {
        if (books.add(book)) {
            book.setId(booksCount);
            booksCount++;
        } else {
            throw new UniquenessException("Wprowadzona książka już istnieje w bazie");
        }
    }

    public void remove(Book[] books) {
        this.books.remove(books);
    }

    public void remove(Collection<Integer> booksIdsToDelete) {
        Iterator<Book> iteratorBook = books.iterator();
        while (iteratorBook.hasNext()) {
            Iterator<Integer> iteratorInteger = booksIdsToDelete.iterator();
            Book book = iteratorBook.next();
            while (iteratorInteger.hasNext()) {
                Integer id = iteratorInteger.next();
                if (book.getId() == id) {
                    iteratorBook.remove();
                }
            }
        }
    }


    public Book[] findTextInAuthorAndTitle(String text) {
        Book[] foundedBooks = new Book[books.size()];
        int count = 0;
        for (Book book : books) {
            if (book != null) {
                if (checkTitle(text, book) || checkAuthor(text, book)) {
                    foundedBooks[count] = book;
                    count++;
                }
            }
        }
        return foundedBooks;
    }

    protected static boolean checkAuthor(String text, Book book) {
        return book.getAuthor().toLowerCase().contains(text.toLowerCase());
    }

    public Collection<Book> getBooks() {
        return books;
    }

    @Override
    public Entity[] getData() {
        return (this.books).toArray(new Entity[0]);
    }

    public void booksSerialization() throws IOException {
        FileOutputStream fos = new FileOutputStream(new File("books"));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        for (Book book : books) {
            oos.writeObject(book);
        }
        oos.close();
        fos.close();
    }

    public void booksDeserialization() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(new File("books"));
        ObjectInputStream ois = new ObjectInputStream(fis);
        boolean endOfFileFlag = false;
        while (!endOfFileFlag) {
            try {
                Book book = (Book) ois.readObject();
                book.setId(booksCount);
                books.add(book);
                booksCount++;
            } catch (EOFException e) {
                endOfFileFlag = true;
            }
        }
        ois.close();
        fis.close();
    }

}
