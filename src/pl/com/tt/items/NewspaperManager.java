package pl.com.tt.items;

import pl.com.tt.exceptions.UniquenessException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static pl.com.tt.enums.ANSIColors.ANSI_RED;
import static pl.com.tt.enums.ANSIColors.ANSI_RESET;

public class NewspaperManager extends ItemManager /*implements RaportDataSourceInterface*/ {

    private int newspaperCount = 0;
    private Collection<Newspaper> newspapers = new ArrayList<>();

    public NewspaperManager() throws IOException {
        try {
            newspapersDeserialization();
        } catch (ClassNotFoundException e) {
            System.out.println("obiekt nie może być zaimportowany - błędna klasa");
        } catch (FileNotFoundException e) {
            System.out.println("brak pliku źródłowego - newspapers");
        }
    }

    public int getNewspaperCount() {
        return newspaperCount;
    }

    public Collection<Newspaper> getNewspapers() {
        return newspapers;
    }

    public void add(Newspaper newspaper) throws UniquenessException {
        if (newspapers.add(newspaper)) {
            newspaper.setId(newspaperCount);
            newspaperCount++;
        } else {
            throw new UniquenessException("Wprowadzone czasopismo już istnieje w bazie");
        }
    }

//    public void remove(int... newspapersIds) {
//        int removeCount = remove(newspapers, newspapersIds);
//        this.newspaperCount = this.newspaperCount - removeCount;
//    }

    public List<Newspaper> findTextInTitleAndTypeOfNewspaper(String text) {
        Newspaper[] foundedNewspapers = new Newspaper[newspapers.size()];
        int count = 0;
        for (Newspaper newspaper : newspapers) {
            if (newspaper != null) {
                if (checkTitle(text, newspaper) || checkTypeOfNewspaper(text, newspaper)) {
                    foundedNewspapers[count] = newspaper;
                    count++;
                }
            }
        }
        return Arrays.asList(foundedNewspapers);
    }

    protected static boolean checkTypeOfNewspaper(String text, Newspaper newspaper) {
        return newspaper.getTypeOfNewspaper().toLowerCase().contains(text.toLowerCase());
    }

    public void newspapersDeserialization() throws IOException, ClassNotFoundException {
        try {
            FileInputStream fis = new FileInputStream(new File("newspapers"));
            ObjectInputStream ois = new ObjectInputStream(fis);

            boolean endOfFileFlag = false;
            while (!endOfFileFlag) {
                try {
                    Newspaper newspaper = (Newspaper) ois.readObject();
                    newspaper.setId(newspaperCount);
                    newspapers.add(newspaper);
                    newspaperCount++;
                } catch (EOFException e) {
                    endOfFileFlag = true;
                }
            }
            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(ANSI_RED + "! błąd importu danych (brak pliku 'newspapers')" + ANSI_RESET);
        } catch (EOFException e) {
            System.out.println(ANSI_RED + "! błąd importu danych z pliku 'newspapers' (" + e.getMessage() + ")" + ANSI_RESET);
        } catch (ClassCastException e) {
            System.out.println(ANSI_RED + "! błąd importu danych z pliku 'newspapers' (" + e.getMessage() + ")" + ANSI_RESET);
        } catch (StreamCorruptedException e) {
            System.out.println(ANSI_RED + "! błąd importu danych z pliku 'newspapers' (" + e.getMessage() + ")" + ANSI_RESET);
//        } catch (IOException e) {

//            tego wyjątku nie trzeba łapać, bo i tak program się wywali
        }
    }
}

