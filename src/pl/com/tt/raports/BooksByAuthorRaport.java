//package pl.com.tt.raports;
//
//import pl.com.tt.entity.Entity;
//import pl.com.tt.items.Book;
//import pl.com.tt.persons.Employee;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//public class BooksByAuthorRaport extends Raport implements RaportInterface {
//
//    private String[] raport;
//
//    BooksByAuthorRaport(String name, Employee employee, String notes) {
//
//        super(name, employee, notes);
//        this.raport = getRaport();  // dlaczego tak?
//    }
//
//    @Override
//    public void doRaport(RaportDataSourceInterface manager) {
//
//        Map<String, Integer> authorsCount = new HashMap<>();
//        Entity[] data = manager.getData();
//        for (Entity entity : data) {
//            if (entity != null) {
//                String author = ((Book) entity).getAuthor();
//                Integer count = authorsCount.get(author);
//                count = count == null ? 1 : count + 1;
//                authorsCount.put(author, count);
//            }
//        }
//
//         this.raport = authorsCount.entrySet().stream()
//                .map(e -> "" + e.getKey() + ": " + e.getValue())
//                .collect(Collectors.toList()).toArray(new String[authorsCount.size()]);
//    }
//
//    @Override
//    public String[] getRaport() {
//        return this.raport;
//    }
//
//
//}
