package pl.com.tt.raports;

import pl.com.tt.entity.Entity;
import pl.com.tt.items.Book;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BooksByStatusRaport implements RaportInterface {

    private String[] raport;

    @Override
    public void doRaport(RaportDataSourceInterface manager) {

        Map<String, Integer> statusCount = new HashMap<>();
        for (Entity entity : manager.getData()) {
            if (entity != null) {
                String status = ((Book) entity).getStatus();
                Integer count = statusCount.get(status);
                count = count == null ? 1 : count + 1;
                statusCount.put(status, count);
            }
        }

        this.raport = statusCount.entrySet().stream()
                .map(e -> "" + e.getKey() + ": " + e.getValue())
                .collect(Collectors.toList()).toArray(new String[statusCount.size()]);

    }

    @Override
    public String[] getRaport() {
        return this.raport;
    }
}
