package pl.com.tt.raports;

import pl.com.tt.entity.Entity;
import pl.com.tt.persons.Employee;

import java.util.Calendar;
import java.util.Date;

public class Raport extends Entity {

    protected String typeOfRaport;
    protected Date dateOfPreparing;
    protected Employee employee;
    protected String notes;

    public Raport(String name, Employee employee, String notes) {
        this.typeOfRaport = name;
        this.employee = employee;
        this.notes = notes;
        this.dateOfPreparing = Calendar.getInstance().getTime();
    }

    public String getName() {
        return typeOfRaport;
    }

    public Date getDate() {
        return dateOfPreparing;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getNotes() {
        return notes;
    }

    @Override
    public String toString() {
        return "Raport id=" + id + " {" +
                "typeOfRaport='" + typeOfRaport +
                ", employee=" + employee +
                ", date=" + dateOfPreparing +
                ", notes=" + notes +
                '}';
    }

}
