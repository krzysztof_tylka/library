package pl.com.tt.raports;

import pl.com.tt.entity.Entity;

public interface RaportDataSourceInterface {

    Entity[] getData();

}
