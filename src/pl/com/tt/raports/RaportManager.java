package pl.com.tt.raports;

import pl.com.tt.entity.Entity;
import pl.com.tt.entity.EntityManager;

public class RaportManager extends EntityManager implements RaportDataSourceInterface {

    private int raportsCount = 0;
    private Raport raports[] = new Raport[100];

    public void addRaport(Raport raport) {

        add(raport, raports, findFirstEmptyPlaceIdx(raports));
        raportsCount++;
    }

    public Raport[] getRaports() {
        return raports;
    }

    public int getRaportsCount() {
        return raportsCount;
    }

    @Override
    public Entity[] getData() {
        return new Entity[raports.length];
    }
}
