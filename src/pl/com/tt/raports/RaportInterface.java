package pl.com.tt.raports;

public interface RaportInterface {

    void doRaport (RaportDataSourceInterface manager);

    String[] getRaport();

}
