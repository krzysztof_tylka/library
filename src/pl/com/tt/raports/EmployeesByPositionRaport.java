package pl.com.tt.raports;

import pl.com.tt.entity.Entity;
import pl.com.tt.persons.Employee;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class EmployeesByPositionRaport implements RaportInterface {

    private Map<String, Integer> mapRaport;

    @Override
    public void doRaport(RaportDataSourceInterface manager) {

        Map<String, Integer> raport = new HashMap<String, Integer>();
        Entity[] entities = manager.getData();

        for (Entity entity : entities) {
            String position = ((Employee) entity).getPosition();
            if (entity != null) {
                if (raport.get(position) != null) {
                    raport.put(position, raport.get(position) + 1);
                } else {
                    raport.put(position, 1);
                }
            }
        }
        mapRaport = raport;
    }

    @Override
    public String[] getRaport() {

        Collection<String> raport = new ArrayList<String>();
        for (Map.Entry<String, Integer> item : mapRaport.entrySet()) {
            raport.add(item.getKey() + " : " + item.getValue());
        }
        return raport.toArray(new String[0]);
    }
}
