package pl.com.tt.raports;

import pl.com.tt.entity.Entity;

public class EmployeeCountRaport implements RaportInterface {

    private String[] raport;

    @Override
    public void doRaport(RaportDataSourceInterface manager) {

        int count = 0;
        for (Entity entity : manager.getData()) {
            if (entity != null) {
                count++;
            }
        }
        String[] numberOfEmployees = new String[1];
        numberOfEmployees[0] = "Liczba pracowników: " + count;
        this.raport = numberOfEmployees;
    }

    @Override
    public String[] getRaport() {
        return this.raport;
    }
}
