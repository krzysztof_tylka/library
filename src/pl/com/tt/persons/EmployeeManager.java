package pl.com.tt.persons;

import pl.com.tt.entity.Entity;
import pl.com.tt.raports.RaportDataSourceInterface;

import java.util.Arrays;

public class EmployeeManager extends PersonManager implements RaportDataSourceInterface {

    private int employeesCount = 0;
    private Employee employees[] = new Employee[5];

    public void add(Employee employee) {

        add(employee, employees, findFirstEmptyPlaceIdx(employees));
        employeesCount++;
    }

    public void remove(int... employeesIds) {

        int removeCount = remove(employees, employeesIds);
        this.employeesCount = this.employeesCount - removeCount;
    }

    public Employee[] findPersonsByTextInNameAndSurname(String text) {

        return (Employee[]) findPersonsByTextInNameAndSurname(employees, text);
    }

    public Employee findById(int id) {
        Entity[] entities = (Entity[])(findByIds(this.employees, id)).toArray();
        if (entities != null && entities.length > 0) {
            return (Employee) entities[0];
        }
        return null;
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    @Override
    public Employee[] getData() {
        return getEmployees();
    }
}
