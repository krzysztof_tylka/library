package pl.com.tt.persons;

import pl.com.tt.entity.EntityManager;

public class PersonManager extends EntityManager {

    public Person[] findPersonsByTextInNameAndSurname(Person[] persons, String text) {

        Person[] findedPersons = new Person[persons.length];
        int count = 0;
        for (Person person : persons) {
            if (person != null) {
                if (checkName(text, person) || checkSurname(text, person)) {
                    findedPersons[count] = person;
                    count++;
                }
            }
        }
        return findedPersons;
    }

    private static boolean checkName(String status, Person person) {
        return person.getName().toLowerCase().contains(status.toLowerCase());
    }

    private static boolean checkSurname(String status, Person person) {
        return person.getSurname().toLowerCase().contains(status.toLowerCase());
    }
}


//    public static Person[] findingByTwoParameters(Person[] persons, String name, String surname) {
//        Person[] findingpersons = new Person[persons.length];
//        int count = 0;
//        for (Person person : persons) {
//            if (person != null) {
//                if (person.getName().equals(name) && person.getSurname().equals(surname)) {
//                    findingpersons[count] = person;
//                    count++;
//                }
//            }
//        }
//        return findingpersons;
//    }
//
//    //    TODO use list
//    public static Person[] findingByOneParameter(Person[] persons, String email) {
//        Person[] findingpersons = new Person[persons.length];
//        int count = 0;
//        for (Person person : persons) {
//            if (person != null) {
//                if (checkTitle(email, person)) {
//                    findingpersons[count] = person;
//                    count++;
//                }
//            }
//        }
//        return findingpersons;
//    }
//
//    public static Person[] findPersonsEverywhereByString(Person[] persons, String word) {
//        Person[] findingPersons = new Person[persons.length];
//        int count = 0;
//        for (Person person : persons) {
//            if (person != null) {
//                if (checkTitle(word, person) || checkAuthor(word, person)) {
//                    findingPersons[count] = person;
//                    count++;
//                }
//            }
//        }
//        return findingPersons;
//    }


