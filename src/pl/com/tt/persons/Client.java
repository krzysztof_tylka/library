package pl.com.tt.persons;

import pl.com.tt.entity.Entity;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Client extends Person {

    private Date dateOfRegistration;
    private String email;
    private Address address;

    public Client(String name, String surname, String email, String city, String street, int streetNumber) {

        super(name, surname);
        this.dateOfRegistration = Calendar.getInstance().getTime();
        this.email = email;
        Address address = new Address(city, street, streetNumber);
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(email, client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return fullname + " | " + address.getCity() + ", " + address.getStreet() + " " + address.getStreetNumber() + " | " + email +
                " (id: " + id + ", dateOfReg: " + dateOfRegistration+")";
    }

    public class Address extends Entity {

        private String city;
        private String street;
        private int streetNumber;
        private String fullAddress;

        public Address(String city, String street, int streetNumber) {

            this.city = city;
            this.street = street;
            this.streetNumber = streetNumber;
            doFullAddress();
        }

        private void doFullAddress() {
            fullAddress = city + ", " + street + " " + streetNumber;
        }

        public String getCity() {
            return city;
        }

        public String getStreet() {
            return street;
        }

        public int getStreetNumber() {
            return streetNumber;
        }
    }

}
