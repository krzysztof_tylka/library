//package pl.com.tt.persons;
//
//import pl.com.tt.entity.Entity;
//
//public class Address extends Entity {
//
//    private String city;
//    private String street;
//    private int streetNumber;
//    private String fullAddress;
//
//    public Address(String city, String street, int streetNumber) {
//
//        this.city = city;
//        this.street = street;
//        this.streetNumber = streetNumber;
//        doFullAddress();
//    }
//
//    private void doFullAddress() {
//        fullAddress = city + ", " + street + " " + streetNumber;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public String getStreet() {
//        return street;
//    }
//
//    public int getStreetNumber() {
//        return streetNumber;
//    }
//
//    public String getFullAddress() {
//        return fullAddress;
//    }
//
//    @Override
//    public String toString() {
//        return fullAddress;
//    }
//
//}
