package pl.com.tt.persons;

import pl.com.tt.exceptions.UniquenessException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static pl.com.tt.enums.ANSIColors.ANSI_RED;
import static pl.com.tt.enums.ANSIColors.ANSI_RESET;

public class ClientManager extends PersonManager {

    public ClientManager() throws IOException {

//        try {
        File file = new File("clients.txt");

        Charset ch = Charset.forName("UTF-8");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), ch))) {
            String textLine;
            while ((textLine = br.readLine()) != null) {
                String[] clientData = textLine.split(",");
                Client client = new Client(clientData[0], clientData[1], clientData[2], clientData[3], clientData[4], Integer.parseInt(clientData[5]));
                client.setId(clientsCount);
                clients.add(client);
                clientsCount++;
            }
        }
//        } catch (IOException e) {
//            System.out.println("Brak pliku źródłowego: clients.txt");
//        }
    }

    private int clientsCount = 0;
    private Collection<Client> clients = new ArrayList<>();

    public void add(Client client) throws UniquenessException {

        if (clients.add(client)) {
            client.setId(clientsCount);
            clientsCount++;
        } else {
            throw new UniquenessException(ANSI_RED + "Klient już istnieje w bazie!" + ANSI_RESET);
        }
    }

    // TODO jak poznam generyki
//        add(client, clients);

    public void remove(Client client) {
        clients.remove(client);
    }

    public void removeAllClients(Collection<Integer> clientIdsToDelete) {

        Iterator<Client> iteratorClient = clients.iterator();
        Iterator<Integer> iteratorInteger = clientIdsToDelete.iterator();

        while (iteratorClient.hasNext()) {
            Client client = iteratorClient.next();
            while (iteratorInteger.hasNext()) {
                Integer id = iteratorInteger.next();
                if (client.getId() == id) {
                    this.clients.remove(client);
                }
            }
        }
    }


//    public void remove(int... clientsIds) {
//
//        int removeCount = remove(clients, clientsIds);
//        this.clientsCount = this.clientsCount - removeCount;
//    }

    public Client[] findPersonsByTextInNameAndSurname(String text) {

        Collection<Client> foundedClients = new ArrayList<>();
        Person[] persons = findPersonsByTextInNameAndSurname(clients.toArray(new Person[0]), text);
        for (Person person : persons) {
            if (person instanceof Client) {
                foundedClients.add((Client) person);
            }
        }
        return foundedClients.toArray(new Client[0]);
    }

    public Collection<Client> getClients() {
        return clients;
    }

}
