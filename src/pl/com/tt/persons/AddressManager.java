//package pl.com.tt.persons;
//
//import pl.com.tt.entity.Entity;
//import pl.com.tt.entity.EntityManager;
//
//public class AddressManager extends EntityManager {
//
//    private int addressCount = 0;
//    private Client.Address[] addresses = new Client.Address[20];
//
//    protected static boolean checkCity(String text, Client.Address address) {
//        return address.getCity().toLowerCase().contains(text.toLowerCase());
//    }
//
//    public void remove(int... addressesIds) {
//
//        int removeCount = remove(addresses, addressesIds);
//        this.addressCount = this.addressCount - removeCount;
//    }
//
//    protected static boolean checkStreet(String text, Client.Address address) {
//        return address.getStreet().toLowerCase().contains(text.toLowerCase());
//    }
//
//    public void add(Client.Address address) {
//
//        add(address, addresses, findFirstEmptyPlaceIdx(addresses));
//        addressCount++;
//    }
//
//    public Client.Address[] findAddressesByTextinCityAndStreet(String text) {
//
//        Client.Address[] findedAddresses = new Client.Address[addresses.length];
//        int count = 0;
//        for (Client.Address address : addresses) {
//            if (address != null) {
//                if (checkCity(text, address) || checkStreet(text, address)) {
//                    findedAddresses[count] = address;
//                    count++;
//                }
//            }
//        }
//        return findedAddresses;
//    }
//
//    public Client.Address findById(int id) {
//        Entity[] entities = findByIds(this.addresses, id);
//        if (entities != null && entities.length > 0) {
//            return (Client.Address) entities[0];
//        }
//        return null;
//    }
//
//    public Client.Address[] getAddresses() {
//        return addresses;
//    }
//
//    public int getAddressCount() {
//        return addressCount;
//    }
//}
