package pl.com.tt.persons;

import pl.com.tt.entity.Entity;

public abstract class Person extends Entity {

    protected String name;
    protected String surname;
    protected String fullname;

    public Person(String name, String surname) {

        this.name = name;
        this.surname = surname;
        doFullName();
    }

    private String doFullName() {
        return fullname = name + " " + surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullname() {
        return fullname;
    }

}
