package pl.com.tt.entity;

import pl.com.tt.items.Item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EntityManager {

    private static boolean checkAuthor(String status, Item item) {
        return item.getStatus().toLowerCase().contains(status.toLowerCase());
    }

    private static boolean checkTitle(String status, Item item) {
        return item.getTitle().toLowerCase().contains(status.toLowerCase());
    }

    private static String getFileName(Entity entity) {

//        if (entity instanceof Address) {
//            return "Address";
//        } else if (entity instanceof Book) {
//            return "Book";
//        } else if (entity instanceof Newspaper) {
//            return "Newspaper";
//        } else if (entity instanceof Client) {
//            return "Client";
//        } else if (entity instanceof Employee) {
//            return "Employee";
//        }
        String className = entity.getClass().getSimpleName();
        System.out.println(className);
        return className;
    }

    public static void add(Entity entity, Entity[] entities, final int targetIndex) {
        entity.setId(targetIndex);
        entities[targetIndex] = entity;
//        saveToFile("file-path" + getFileName(entity) + ".csv", entity.toCsv());
    }

    public static void add(Entity entity, Collection<Entity> entities) {
        entities.add(entity);
    }

    public static void addToList(Entity entity, Entity[] entities, final int targetIndex) {
        entity.setId(targetIndex);
        entities[targetIndex] = entity;
//        saveToFile("file-path" + getFileName(entity) + ".csv", entity.toCsv());
    }

    public static int findFirstEmptyPlaceIdx(Entity[] entities) {

        for (int i = 0; i < entities.length; i++) {
            if (entities[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public static int remove(Entity[] entities, int[] ids) {
        int count = 0;
        for (int i = 0; i < entities.length; i++) {
            if (entities[i] != null) {
                for (int id : ids) {
                    if (entities[i].getId() == (id)) {
                        entities[i] = null;
                        count++;
                        break;
                    }
                }
            }
        }
        return count;

    }

    public static List<Entity> findByIds(Entity[] entities, int... ids) {
        List<Entity> foundedEntities = new ArrayList<>();
        int count = 0;
        for (Entity entity : entities) {
            if (entity != null) {
                for (int id : ids) {
                    if (entity.getId() == id) {
                        foundedEntities.set(count, entity);
                        count++;
                        break;
                    }
                }
            }
        }
        return foundedEntities;
    }


    private static void saveToFile(String fileName, String data) {
        // nope
    }
}
