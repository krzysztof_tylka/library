package pl.com.tt.entity;

import java.util.Collection;

public abstract class Entity {

    protected int id;

    public Entity() {
    }

//    public abstract String toCsv();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
