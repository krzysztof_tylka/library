package pl.com.tt.enums;

public enum TypeOfNewspaper {

    WEEKLY_NEWSPAPER("tygodnik"),
    MONTHLY_NEWSPAPER("miesięcznik"),
    QUARTERLY_NEWSPAPER("kwartalnik");

    private String typeOfNewspaper;

    TypeOfNewspaper(String typeOfNewspaper) {
        this.typeOfNewspaper = typeOfNewspaper;
    }

    public String getTypeOfNewspaper() {
        return typeOfNewspaper;
    }
}
