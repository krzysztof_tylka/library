package pl.com.tt.enums;

import pl.com.tt.raports.EmployeeCountRaport;

public enum EmployeePosition {

    LEADER("kierownik"),
    TRAINEE("stażysta"),
    SPECIALIST("specjalista");

    private String employeePosition;

    EmployeePosition(String employeePosition) {
        this.employeePosition = employeePosition;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }
}
