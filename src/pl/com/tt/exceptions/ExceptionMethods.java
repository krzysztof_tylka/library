package pl.com.tt.exceptions;

import pl.com.tt.enums.ANSIColors;

public class ExceptionMethods {

    public static void printMessageError(BookException e) {
        System.out.println(ANSIColors.ANSI_CYAN + e.getMessage() + ANSIColors.ANSI_RESET);
    }

    public static void printMessageError(Exception e) {
        System.out.println(ANSIColors.ANSI_CYAN + e.getMessage() + ANSIColors.ANSI_RESET);
    }
}
