package pl.com.tt.exceptions;

public class NewspaperException extends Exception {

    public NewspaperException(String msg) {
        super(msg);
    }

    public NewspaperException(String msg, Exception e) {
        super(msg, e);
    }

}
