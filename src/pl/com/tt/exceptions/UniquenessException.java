package pl.com.tt.exceptions;

public class UniquenessException extends Exception {

    public UniquenessException(String msg) {
        super(msg);
    }

    public UniquenessException(String msg, Exception e) {
        super(msg, e);
    }

}
