package pl.com.tt.exceptions;

public class BookException extends Exception {

    public BookException(String msg) {
        super(msg);
    }

    public BookException(String msg, Exception e) {
        super(msg, e);
    }

}
