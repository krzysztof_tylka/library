package pl.com.tt.newsletter;

import pl.com.tt.persons.Client;

public class Mailer {

    public static String prepareEmail(Client[] clients) {

        StringBuilder listOfEmails = new StringBuilder();

        for (Client client : clients) {
            if (client != null) {
//                listOfEmails += client.getName()+"-"+client.getSurname() + "@gmail.com, ";
//                listOfEmails += client.getFullname().replace(" ", "-") + "@gmail.com, ";
                listOfEmails.append(client.getFullname().replace(" ", "_") + "@gmail.com\n");
            }
        }

        return listOfEmails.substring(0, listOfEmails.length() - 1);


    }

}
