package pl.com.tt.main;

import pl.com.tt.console.*;
import pl.com.tt.exceptions.UniquenessException;
import pl.com.tt.items.Book;
import pl.com.tt.items.Newspaper;
import pl.com.tt.persons.Client;
import pl.com.tt.utils.Methods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static pl.com.tt.enums.ANSIColors.*;
import static pl.com.tt.utils.TextsPL.NO_RESULTS;
import static pl.com.tt.utils.TextsPL.UNDER_CONSTRUCTION;


public class Menu {

    public static void printMenu(Library library) throws IOException {

        int firstLevelMenuChoice;
        int secondLevelMenuChoice;
        int thirdLevelMenuChoice;
        String choice4;
        System.out.println("  __________________.");
        System.out.println(" /                 /|");
        System.out.println("/_________________/ |");
        System.out.println("|                |  |");
        System.out.println("| Biblioteka 1.0 | /");
        System.out.println("|________________|/");


        do {
            Methods.printRequestForMakingChoice();
            System.out.println("1. Klienci");
            System.out.println("2. Zarejestruj wypożyczenie");
            System.out.println("3. Zarejestruj zwrot");
            System.out.println("4. Zasoby biblioteczne");
            System.out.println("5. Raporty");
            System.out.println("6. Ustawienia");
            System.out.println("0. Wyjście");
            firstLevelMenuChoice = ConsoleManager.putIntegerData();
            switch (firstLevelMenuChoice) {
                case 1:
                    do {
                        Methods.printRequestForMakingChoice();
                        System.out.println("1. Dodaj klienta");
                        System.out.println("2. Usuń klienta");
                        System.out.println("3. Wyświetl bazę klientów");
                        System.out.println("0. Powrót");
                        secondLevelMenuChoice = ConsoleManager.putIntegerData();
                        switch (secondLevelMenuChoice) {
                            case 1: // dodawanie klientów
                                try {
                                    library.addClient(InputClientDataChecking.checkingData());
                                } catch (UniquenessException e) {
                                    System.out.println(e);
                                }
                                break;
                            case 2: // usuwanie klientów
                                if (library.getClientManager().getClients().size() == 0) {
                                    System.out.println("Baza klientów jest pusta.");
                                    System.out.println("Najpierw dodaj klienta do bazy.");
                                } else {
                                    Client[] foundedClients = (library.findClientsByTextInNameAndSurname(InputClientDataSearching.checkingData()));
                                    if (foundedClients.length == 0) {
                                        System.out.println(NO_RESULTS);
                                    } else {
                                        Main.show(foundedClients);
                                        Collection<Integer> clientsToDelete = new ArrayList<>();
                                        do {
                                            System.out.println("\nPodaj nr ID klienta, którego chcesz usunąć:");
                                            clientsToDelete.add(ConsoleManager.putIntegerData());
                                            System.out.println("Następny ID? (t/n)");
                                            choice4 = ConsoleManager.putStringData();

                                        }
                                        while (!choice4.equals("n"));
                                        library.removeAllClients(clientsToDelete);
                                    }
                                }
//                                Main.show(library.getClientManager().getClients().toArray());
                                break;
                            case 3: //wyświetanie bazy klientów
                                if (library.getClientManager().getClients().size() == 0) {
                                    System.out.println("Baza klientów jest pusta.");
                                } else {
                                    Main.show(library.getClientManager().getClients().toArray());
                                }
                            default:
                                if (secondLevelMenuChoice < 0 || secondLevelMenuChoice > 6)
                                    System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
                        }
                    }
                    while (secondLevelMenuChoice != 0);
                    break;

                case 2: // rejestracja wypożyczeń
//                    library.addOperation(OperationDataChecking.checkingOperationDataFormat());
                    System.out.println(UNDER_CONSTRUCTION);
//                    Main.show(library.getOperationManager().getOperations());
                    break;

                case 3: // rejestracja zwrotów
                    System.out.println(UNDER_CONSTRUCTION);
                    break;

                case 4: // zasoby biblioteczne
                    do {
                        Methods.printRequestForMakingChoice();
                        System.out.println("1. Dodaj książkę");
                        System.out.println("2. Usuń książkę");
                        System.out.println("3. Dodaj czasopismo");
                        System.out.println("4. Usuń czasopismo");
                        System.out.println("5. Wyświetl listę książek");
                        System.out.println("6. Wyświetl listę czasopism");
                        System.out.println("0. Powrót");
                        secondLevelMenuChoice = ConsoleManager.putIntegerData();
                        switch (secondLevelMenuChoice) {
                            case 1:
                                try {
                                    library.addBook(InputBookDataChecking.checkingData());
                                } catch (UniquenessException e) {
                                    System.out.println(e);
                                }
                                break;
                            case 2:
                                if (library.getBookManager().getBooks().size() == 0) {
                                    System.out.println("Baza książek jest pusta.");
                                    System.out.println("Najpierw dodaj książkę do bazy.");
                                } else {
                                    Book[] foundedBooks = (library.findBooksByTextinAuthorAndTitle((InputClientDataSearching.checkingData())));
                                    if (foundedBooks.length == 0) {
                                        System.out.println(NO_RESULTS);
                                    } else {
                                        Main.show(foundedBooks);
                                        Collection<Integer> booksToDelete = new ArrayList<>();

                                        do {
                                            System.out.println("\nPodaj nr ID książki, którą chcesz usunąć:");
                                            booksToDelete.add(ConsoleManager.putIntegerData());
                                            System.out.println("Następny ID? (t/n)");
                                            choice4 = ConsoleManager.putStringData();
                                        }
                                        while (!choice4.equals("n"));
                                        library.removeBooks(booksToDelete);
                                    }
                                }
                                break;
                            case 3:
                                do {
                                    Methods.printRequestForMakingChoice();
                                    System.out.println("1. Dodaj pojedyncze czasopismo z konsoli");
                                    System.out.println("2. Dodaj wiele czasopism z pliku");
                                    System.out.println("0. Powrót");
                                    thirdLevelMenuChoice = ConsoleManager.putIntegerData();
                                    switch (thirdLevelMenuChoice) {
                                        case 1:
                                            try {
                                                library.addNewspaper(InputNewspaperDataChecking.checkingData());
                                            } catch (UniquenessException e) {
                                                System.out.println(e);
                                            }
                                            break;
                                        case 2:
                                            System.out.println(UNDER_CONSTRUCTION);
                                            break;
                                        default:
                                            if (thirdLevelMenuChoice < 0 || thirdLevelMenuChoice > 6)
                                                System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
                                    }
                                }
                                while (thirdLevelMenuChoice != 0);
                                break;
                            case 4:
                                List<Newspaper> newspapersToDelete = (library.findNewspapersByTextInTitleAndTypeOfNewspaper(InputBookDataSearching.checkingData()));
                                break;
                            case 5:
//                                TreeSet<Book> set = new TreeSet<Book>(new BookComparator());
                                Main.show(library.getBookManager().getBooks().toArray());

                                break;
                            case 6:
                                Main.show(library.getNewspaperManager().getNewspapers().toArray());
                                break;
                            default:
                                if (secondLevelMenuChoice < 0 || secondLevelMenuChoice > 6)
                                    System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
                        }
                    }
                    while (secondLevelMenuChoice != 0);
                    break;

                case 5: // raporty
                    do {
                        Methods.printRequestForMakingChoice();
                        System.out.println("1. Raport aaa");
                        System.out.println("2. Raport bbb");
                        System.out.println("0. Powrót");
                        secondLevelMenuChoice = ConsoleManager.putIntegerData();
                        switch (secondLevelMenuChoice) {
                            case 1:
                                System.out.println(UNDER_CONSTRUCTION);
                                break;
                            case 2:
                                System.out.println(UNDER_CONSTRUCTION);
                                break;
                            default:
                                if (secondLevelMenuChoice < 0 || secondLevelMenuChoice > 2)
                                    System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
                        }
                    }
                    while (secondLevelMenuChoice != 0);
                    break;

                case 6: // ustawienia
                    do {
                        Methods.printRequestForMakingChoice();
                        System.out.println("1. Personel biblioteki");
                        System.out.println("2. Administracja");
                        System.out.println("0. Powrót");
                        secondLevelMenuChoice = ConsoleManager.putIntegerData();
                        switch (secondLevelMenuChoice) {
                            case 1: // personel
                                System.out.println(UNDER_CONSTRUCTION);
                                break;
                            case 2: // zarządzanie dostępem
                                System.out.println(UNDER_CONSTRUCTION);
                                break;
                            default:
                                if (secondLevelMenuChoice < 0 || secondLevelMenuChoice > 2)
                                    System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
                        }
                    }
                    while (secondLevelMenuChoice != 0);
                    break;
                default:
                    if (firstLevelMenuChoice < 0 || firstLevelMenuChoice > 6)
                        System.out.println(ANSI_RED + "! błędny wybór" + ANSI_RESET);
            }
        }
        while (firstLevelMenuChoice != 0);
        library.close();
        System.out.println(ANSI_PURPLE + "Baza książek została zaktualizowana" + ANSI_RESET);
    }
}
