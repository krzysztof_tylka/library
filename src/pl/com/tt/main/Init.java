package pl.com.tt.main;

import pl.com.tt.exceptions.BookException;
import pl.com.tt.items.Book;
import pl.com.tt.items.Newspaper;
import pl.com.tt.persons.Employee;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Init {

//    public static void initBookByConsole(Library library) {
//        Scanner data = new Scanner(System.in);
//        String title;
//        String author;
//        int number;
//        int year;
//        int isbn;
//
//        do {
//            try {
//                Init.printText("tytuł");
//                title = data.nextLine();
//                Init.printText("autora");
//                author = data.nextLine();
//                do {
//                    try {
//                        Init.printText("nr wydania");
//                        number = data.nextInt();
//                        data.next();
//                        break;
//                    } catch (InputMismatchException e) {
//                        System.out.println("Niepoprawny format danych");
//                    }
//                } while (true);
//
//                Init.printText("rok wydania");
//                year = data.nextInt();
//                Init.printText("isbn");
//                isbn = data.nextInt();
//                Book book = new Book(title, author, number, year, isbn);
//                library.addBook(book);
//                System.out.println(book);
//
//            } finally {
//
//                String choice;
//                System.out.println("Kontynuować? (t/n)");
//                choice = data.nextLine();
//                System.out.println(choice);
//                if (!choice.equals("n")) {
//                    break;
//                }
//            }
//
//        } while (data.hasNext());
//    }

    public static void printText(String text) {

        System.out.println("Podaj " + text);
    }

//    public static void initBooks(Library library, int howMany) throws BookException {
//        int isbn = 11000;
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
//            Book b = new Book("A" + idx, "Henryk Sienkiewicz", idx, 1985, isbn);
//            library.addBook(b);
//            isbn++;
//        }
//    }

//    public static void initBooks2(Library library, int howMany) {
//        int isbn = 2220;
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
//            Book b = new Book("Manitou " + idx, "Graham Masterton", idx, 2011 + idx, isbn);
//            library.addBook(b);
//            isbn++;
//        }
//    }

//    public static void initBooks3(Library library, int howMany) {
//        int isbn = 5500;
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
//            Book b = new Book("Pan Tadeusz " + idx, "Adam Mickiewicz", idx, 1999, isbn);
//            library.addBook(b);
//            isbn++;
//        }
//    }

//    public static void initNewspapers(Library library, int howMany) {
//        int isbn = 35000;
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
//            Newspaper n = new Newspaper("Cosmopolitan", "XYZ", 2014, 4, isbn);
//            library.addNewspaper(n);
//            isbn += 55;
//        }
//    }

//    public static void initEmployees(Library library, int howMany) {
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
////            int addressId = i % library.getAddressManager().getAddressCount();
////            Address address = library.findAddress(addressId);
//            Employee e = new Employee("Anna", "Nowak" + idx);
////            c.setEmail(c.getFullname()+"@gmail.com");
//            library.addEmployee(e);
//        }
    }

//    public static void initClients(Library library, int howMany) {
//        for (int i = 0; i < howMany; i++) {
//            int idx = i + 1;
//            int addressId = i % library.getAddressManager().getAddressCount();
//            Client c = new Client("Piotr", "Stępień" + idx, "");
//            c.setEmail(c.getName() + c.getSurname() + "@gmail.com");
//            Address address = library.findAddress(addressId);
//            library.addClient(c);
//        }
//    }
//
//    public static void initAddresses(Library library) {
//        Client client = new Client("test", "test", "test");
//        library.addAddress(client.new Address("Warszawa", "Polna", 20));
//        library.addAddress(client.new Address("Warszawa", "Polna", 20));
//        Client.Address a = client.new Address("Warszawa", "Dobra", 2);
//        library.addAddress(a);
//        a = client.new Address("Warszawa", "Miła", 11);
//        library.addAddress(a);
//        a = client.new Address("Warszawa", "Piękna", 225);
//        library.addAddress(a);
//        a = client.new Address("Warszawa", "Kocia", 144);
//        library.addAddress(a);
//        a = client.new Address("Warszawa", "Brzydka", 55);
//        library.addAddress(a);
//
//    }

//}
