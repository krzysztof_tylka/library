package pl.com.tt.main;

import jdk.nashorn.internal.runtime.ListAdapter;
import pl.com.tt.exceptions.UniquenessException;
import pl.com.tt.items.Book;
import pl.com.tt.items.BookManager;
import pl.com.tt.items.Newspaper;
import pl.com.tt.items.NewspaperManager;
import pl.com.tt.operations.Operation;
import pl.com.tt.operations.OperationManager;
import pl.com.tt.persons.Client;
import pl.com.tt.persons.ClientManager;
import pl.com.tt.persons.Employee;
import pl.com.tt.persons.EmployeeManager;
import pl.com.tt.raports.Raport;
import pl.com.tt.raports.RaportManager;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class Library {

    private BookManager bookManager = new BookManager();
    private NewspaperManager newspaperManager = new NewspaperManager();
    private ClientManager clientManager = new ClientManager();
    private EmployeeManager employeeManager = new EmployeeManager();
    private OperationManager operationManager = new OperationManager();
    private RaportManager raportManager = new RaportManager();

    public Library() throws IOException {
    }

    public BookManager getBookManager() {
        return bookManager;
    }

    public NewspaperManager getNewspaperManager() {
        return newspaperManager;
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public EmployeeManager getEmployeeManager() {
        return employeeManager;
    }

    public OperationManager getOperationManager() {
        return operationManager;
    }

    public RaportManager getRaportManager() {
        return raportManager;
    }

    public void addBook(Book book) throws UniquenessException {
        this.bookManager.add(book);
    }

    public void removeBooks(Collection<Integer> booksToDelete) {
        this.bookManager.remove(booksToDelete);
    }

    public Book[] findBooksByTextinAuthorAndTitle(String text) {
        return this.bookManager.findTextInAuthorAndTitle(text);
    }

    public void addNewspaper(Newspaper newspaper) throws UniquenessException {
        this.newspaperManager.add(newspaper);
    }

//    public void removeNewspapers(Collection<Integer> newspapersToDelete) {
//        this.newspaperManager.remove(newspapersToDelete);
//    }

    public List<Newspaper> findNewspapersByTextInTitleAndTypeOfNewspaper(String text) {
        return this.newspaperManager.findTextInTitleAndTypeOfNewspaper(text);
    }

    public void addClient(Client client) throws UniquenessException {
        this.clientManager.add(client);
    }

    public void removeAllClients(Collection<Integer> clientsToDelete) {
        this.clientManager.removeAllClients(clientsToDelete);
    }

    public Client[] findClientsByTextInNameAndSurname(String text) {
        return this.clientManager.findPersonsByTextInNameAndSurname(text);
    }

    public void addEmployee(Employee employee) throws UniquenessException {
        this.employeeManager.add(employee);
    }

    public void removeEmployees(int... employeesIds) {
        this.employeeManager.remove(employeesIds);
    }

    public void addOperation(Operation operation) {
        this.operationManager.add(operation);
    }

    public void addRaport(Raport raport) {
        this.raportManager.addRaport(raport);
    }

    public void close() throws IOException {
        this.bookManager.booksSerialization();
    }

}


