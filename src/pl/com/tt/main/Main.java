package pl.com.tt.main;

import pl.com.tt.exceptions.BookException;
import pl.com.tt.exceptions.UniquenessException;
import pl.com.tt.utils.Methods;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws BookException, IOException, UniquenessException {

        Library library = new Library();

        Menu.printMenu(library);

//        Init.initAddresses(library);
//        Init.initBookByConsole(library);
//        Init.initBooks3(library, 5);
//        Init.initBooks2(library, 5);
//        Init.initBooks(library, 5);
//        Init.initClients(library, 15);
//        Init.initNewspapers(library, 15);
//        Init.initEmployees(library, 5);
//        library.removeBooks(0, 5, 11, 14);
//        Init.initBooks(library, 7);
//        library.removeBooks(1, 4, 11, 15, 17);
//        Init.initBooks2(library, 8);
//        library.removeBooks(5, 7, 10, 18, 19);
//        show(library.getBookManager().getBooks());
//        System.out.println();
//        show(library.getEmployeeManager().getEmployees());
//        show(library.getAddressManager().getAddresses());
//        System.out.println();
//        show(library.getAddressManager().getAddresses());
//        show(library.getBookManager().getBooks());
//        show(library.getClientManager().getClients());
//        show(library.getEmployeeManager().getEmployees());
//        show(library.getNewspaperManager().getNewspapers());
//
//        BooksByStatusRaport bookStatusRaport = new BooksByStatusRaport();
//        bookStatusRaport.doRaport(library.getBookManager());
//
//        EmployeeCountRaport employeeCountRaport = new EmployeeCountRaport();
//        employeeCountRaport.doRaport(library.getEmployeeManager());
//
//        EmployeesByPositionRaport employeeByPositionRaport = new EmployeesByPositionRaport();
//        employeeByPositionRaport.doRaport(library.getEmployeeManager());
//
//        ConsoleRaportPrinter consoleRaportPrinter = new ConsoleRaportPrinter();
//        consoleRaportPrinter.show(bookByAuthorRaport);
//        consoleRaportPrinter.show(bookStatusRaport);
//        consoleRaportPrinter.show(fakeAverageRaport);
//        consoleRaportPrinter.show(employeeCountRaport);
//        consoleRaportPrinter.show(employeeByPositionRaport);
    }

    public static void show(Object[] entities) {

        int count = 0;
        if (entities.length == 0 || entities[0] == null) {
            Methods.printNoSearchingResultsText();
            Methods.printNextLine();
        }
        for (Object entity : entities) {
            if (entity != null) {
                System.out.println((count + 1) + ".\t" + entities[count]);
                count++;
            }
        }
    }


    public static void show2(Object[][] entities) {

        int count = 0;
        for (Object entity : entities) {
            if (entity != null) {
                System.out.println(count + ".\t" + entities[count][1] + "\t" + entities[count][2]);
                count++;
            }
        }
    }

}






