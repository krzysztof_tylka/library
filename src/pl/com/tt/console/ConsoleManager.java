package pl.com.tt.console;

import com.sun.media.sound.InvalidFormatException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static pl.com.tt.enums.ANSIColors.ANSI_RED;
import static pl.com.tt.enums.ANSIColors.ANSI_RESET;

public class ConsoleManager {

    public static String putStringData(String text) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Podaj " + text + ": ");
        return br.readLine();
    }

    public static String putStringData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return br.readLine();
    }

    public static int putIntegerData(String text) throws IOException, NumberFormatException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Podaj " + text + ": ");
        String data = br.readLine();
        int number;
        try {
            number = Integer.parseInt(data);
        } catch (NumberFormatException nfe) {
            throw new InvalidFormatException(ANSI_RED + "Błędny format danych!" + ANSI_RESET);
        }
        return number;
    }

    public static int putIntegerData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int putData = -1;
        try {
            String data = br.readLine();
            putData = Integer.parseInt(data);
        } catch (NumberFormatException e) {}
        return putData;
    }
}
