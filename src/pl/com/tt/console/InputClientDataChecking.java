package pl.com.tt.console;

import com.sun.media.sound.InvalidFormatException;
import pl.com.tt.enums.ANSIColors;
import pl.com.tt.exceptions.BookException;
import pl.com.tt.exceptions.ExceptionMethods;
import pl.com.tt.persons.Client;
import pl.com.tt.utils.Methods;

import static pl.com.tt.utils.TextsPL.*;

import java.io.IOException;

public class InputClientDataChecking {

    public static Client checkingData() throws IOException {

        String name = null;
        String surname = null;
        String email = null;
        String city = null;
        String street = null;
        int numberStreet = -1;

        while (name == null || name.trim().isEmpty()) {
            try {
                name = ConsoleManager.putStringData(NAME);
                if (name == null || name.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED.getColorCode() + "Niepoprawnie wprowadzone " + NAME + "!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW + NAME1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (surname == null || surname.trim().isEmpty()) {
            try {
                surname = ConsoleManager.putStringData(SURNAME);
                if (surname == null || surname.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED + "Niepoprawnie wprowadzone " + SURNAME + "!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + SURNAME1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (email == null || email.trim().isEmpty()) {
            try {
                email = ConsoleManager.putStringData(EMAIL);
                if (email == null || email.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED + "Niepoprawnie wprowadzony " + EMAIL + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + EMAIL1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (city == null || city.trim().isEmpty()) {
            try {
                city = ConsoleManager.putStringData(CITY);
                if (city == null || city.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED + "Niepoprawnie wprowadzony " + CITY + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + CITY1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (street == null || street.trim().isEmpty()) {
            try {
                street = ConsoleManager.putStringData(STREET);
                if (street == null || street.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED + "Niepoprawnie wprowadzony " + STREET + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + STREET1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (numberStreet == -1) {
            try {
                numberStreet = ConsoleManager.putIntegerData(STREET_NUMBER);
                if (numberStreet == -1) {
                    throw new InvalidFormatException(ANSIColors.ANSI_RED + "Błędny format danych!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (InvalidFormatException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + STREET_NUMBER1 + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        return new Client(name, surname, email, city, street, numberStreet);
    }
}

