package pl.com.tt.console;

import pl.com.tt.enums.ANSIColors;
import pl.com.tt.exceptions.BookException;
import pl.com.tt.exceptions.ExceptionMethods;
import pl.com.tt.utils.Methods;

import java.io.IOException;

import static pl.com.tt.utils.TextsPL.*;

public class InputClientDataSearching {

    public static String checkingData() throws IOException {

        String text = null;

        while (text == null || text.trim().isEmpty()) {
            try {
                text = ConsoleManager.putStringData(SEARCHING_TEXT);
                if (text == null || text.trim().isEmpty()) {
                    throw new BookException(ANSIColors.ANSI_RED.getColorCode() + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (BookException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW + "Fraza wyszukiwania" + AT_LEAST_ONE_CHAR + TRY_AGAIN + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        return text;
    }
}

