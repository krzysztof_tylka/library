package pl.com.tt.console;

import pl.com.tt.raports.RaportInterface;

public class ConsoleRaportPrinter {

    public void show (RaportInterface raportInterface) {

        System.out.println("RaportInterface - " + raportInterface.getClass().getSimpleName());

        int count = 1;
        for (Object entity : raportInterface.getRaport()) {
            if (entity != null) {
                System.out.print(count + ".\t");
                System.out.println(entity);
                count++;

            }
        }
    }


}
