package pl.com.tt.console;

import com.sun.media.sound.InvalidFormatException;
import pl.com.tt.enums.ANSIColors;
import pl.com.tt.exceptions.NewspaperException;
import pl.com.tt.exceptions.ExceptionMethods;
import pl.com.tt.items.Newspaper;
import pl.com.tt.utils.Methods;

import java.io.IOException;

import static pl.com.tt.utils.TextsPL.*;

public class InputNewspaperDataChecking {

    public static Newspaper checkingData() throws IOException {

        String title = null;
        String publisher = null;
        int yearOfPublish = -1;
        int month = -1;
        int isbn = -1;

        while (title == null || title.trim().isEmpty()) {
            try {
                title = ConsoleManager.putStringData(TITLE);
                if (title == null || title.trim().isEmpty()) {
                    throw new NewspaperException(ANSIColors.ANSI_RED.getColorCode() + "Niepoprawnie wprowadzony " + TITLE + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (NewspaperException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW + "Tytuł musi zawierać przynajmniej jeden znak. Wprowadź tytuł ponownie!" + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (publisher == null || publisher.trim().isEmpty()) {
            try {
                publisher = ConsoleManager.putStringData(PUBLISHER);
                if (publisher == null || publisher.trim().isEmpty()) {
                    throw new NewspaperException(ANSIColors.ANSI_RED + "Niepoprawnie wprowadzony " + AUTHOR + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (NewspaperException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + "Autor musi zawierać przynajmniej jeden znak. Wprowadź autora ponownie!" + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (yearOfPublish == -1) {
            try {
                yearOfPublish = ConsoleManager.putIntegerData(YEAR_OF_PUBLISH);
                if (yearOfPublish < 1) {
                    throw new InvalidFormatException(ANSIColors.ANSI_RED + "Błędny format danych!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (InvalidFormatException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + "Nr wydania musi składać się wyłącznie z cyfr. Wprowadź nr wydania ponownie!" + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (month == -1) {
            try {
                month = ConsoleManager.putIntegerData(MONTH);
                if (month == -1) {
                    throw new InvalidFormatException(ANSIColors.ANSI_RED + "Błędny format danych!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (InvalidFormatException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + "Nr wydania musi składać się wyłącznie z cyfr. Wprowadź nr wydania ponownie!" + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }
        while (isbn == -1) {
            try {
                isbn = ConsoleManager.putIntegerData(ISBN);
                if (isbn == -1) {
                    throw new InvalidFormatException(ANSIColors.ANSI_RED + "Błędny format danych!" + ANSIColors.ANSI_RESET.getColorCode());
                }
            } catch (InvalidFormatException e) {
                ExceptionMethods.printMessageError(e);
                System.out.println(ANSIColors.ANSI_YELLOW.getColorCode() + "Nr wydania musi składać się wyłącznie z cyfr. Wprowadź nr wydania ponownie!" + ANSIColors.ANSI_RESET.getColorCode());
                Methods.printNextLine();
            }
        }

        return new Newspaper(title, publisher, yearOfPublish, month, isbn);

    }


}

