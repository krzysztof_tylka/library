package pl.com.tt.fake_to_delete;

import pl.com.tt.fake_to_delete.FakeEntity;
import pl.com.tt.entity.Entity;
import pl.com.tt.raports.RaportDataSourceInterface;
import pl.com.tt.raports.RaportInterface;

public class FakeAverageRaport implements RaportInterface {

    private String[] report;

    @Override
    public void doRaport(RaportDataSourceInterface manager) {

        float avg = 0f;
        int count = 0;
        for (Entity entity : manager.getData()) {
            if(entity != null) {
                FakeEntity fakeEntity = (FakeEntity) entity;
                avg += fakeEntity.getNr();
                count ++;
            }
        }
        avg = avg/count;

        this.report = new String[]{"Avg = " + avg};
    }

    @Override
    public String[] getRaport() {
        return this.report;
    }
}
