package pl.com.tt.fake_to_delete;

import pl.com.tt.entity.Entity;
import pl.com.tt.raports.RaportDataSourceInterface;

public class FakeRaportDataSource implements RaportDataSourceInterface {

    @Override
    public Entity[] getData() {
        FakeEntity[] entities = new FakeEntity[5];
        entities[0] = new FakeEntity(3);
        entities[1] = new FakeEntity(5);
        entities[2] = new FakeEntity(18);
        entities[3] = new FakeEntity(33);
        entities[4] = new FakeEntity(31);
        return entities;
    }
}
